// Array de prefijos
const prefixes = ['Ae', 'Bae', 'Dae', 'Elae', 'Gae', 'Helae', 'Jaehae', 'Mata', 'Mae', 'Nae', 'Rhae', 'Sae', 'Shae', 'Va', 'Vale', 'Vise'];

// Array de sufijos para hombres
const maleSuffixes = ['gar', 'gel', 'gon', 'gor', 'kar', 'larr', 'lon', 'lor', 'lyx', 'mion', 'mon', 'mond', 'nar', 'nys', 'rion', 'ron', 'rys'];

// Array de sufijos para mujeres
const femaleSuffixes = ['gelle', 'l', 'la', 'lla', 'lle', 'lora', 'na', 'nerys', 'nora', 'nya', 'nyra', 'nys', 'ra', 'rea', 'rra', 'rys'];

function generateName(gender) {
  let suffixes;

  // Seleccionar el array de sufijos correcto en función del género seleccionado
  if (gender === 'male') {
    suffixes = maleSuffixes;
  } else if (gender === 'female') {
    suffixes = femaleSuffixes;
  }

  // Seleccionar aleatoriamente un prefijo y un sufijo
  const randomPrefix = prefixes[Math.floor(Math.random() * prefixes.length)];
  const randomSuffix = suffixes[Math.floor(Math.random() * suffixes.length)];

  // Mostrar el resultado en la página
  document.getElementById('result').innerHTML = randomPrefix + randomSuffix;
}
